# E-commerce Challenge
Front-end App Page to manage a simple E-commerce site with products and a shopping cart.
Deployed with ```now``` 

Here: [e-commerce](https://ecommercechallenge-xgxiitwgba.now.sh/)

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).


## Prerequisites

You need git to clone this repository. You can install git from
[http://git-scm.com/](http://git-scm.com/).

The development tools provided require node.js. You must have node.js and
its package manager (npm) installed.  You can get them from [http://nodejs.org/](http://nodejs.org/).

## Clone ecommerce_challenge 

Clone the ecommerce_challenge  repository using git:

```
git clone https://bitbucket.org/parziee/ecommerce_challenge
cd ecommerce_challenge
```

## Install Dependencies

This project uses npm dependencies. Too install them run the following:

```
npm install 
```

## How to run the App

In the project directory, you can run:

```
npm start
````

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>

```
npm run build
````

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>

## Markup and Styles

For this web app we used both SASS and Styled Components to stylize some components in the same component file.

## Architecture

We Used React Redux to try to follow best practices, keeping control of the state in separate files avoiding passing props trough the components and trying to make the application more maintainable, extensible, flexible and reusable.
