const categories =
    [
        {
            "id": 1,
            "name": "Bebidas",
            "sublevels": [
                {
                    "id": 14,
                    "name": "Gaseosas",
                    "sublevels": [
                        {
                            "id": 2,
                            "name": "Con azúcar"
                        },
                        {
                            "id": 3,
                            "name": "Sin azúcar"
                        }
                    ]
                }
            ]
        },
        {
            "id": 15,
            "name": "Desayunos",
            "sublevels": [
                {
                    "id": 4,
                    "name": "Colombianos",
                    "sublevels": [
                        {
                            "id": 5,
                            "name": "Caldos"
                        },
                        {
                            "id": 6,
                            "name": "Calentados",
                            "sublevels": [
                                {
                                    "id": 7,
                                    "name": "Calentados Vegetarianos"
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            "id": 8,
            "name": "Almuerzos",
            "sublevels": [
                {
                    "id": 9,
                    "name": "Comidas Rápidas"
                },
                {
                    "id": 10,
                    "name": "Almuerzos Caseros"
                }
            ]
        },
        {
            "id": 11,
            "name": "Vinos",
            "sublevels": [
                {
                    "id": 12,
                    "name": "Vino Blanco"
                },
                {
                    "id": 13,
                    "name": "Vino Tinto"
                }
            ]
        }
    ]

module.exports = {categories};