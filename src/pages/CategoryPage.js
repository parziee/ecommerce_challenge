import React, { Component } from 'react';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';
import NavBar from '../components/NavBar/NavBar';
import ShoppingCart from '../components/ShoppingCart/ShoppingCart';
import CategoryContainer from '../components/Category/CategoryContainer';
import '../styles/Main.css';
import { connect } from 'react-redux';
import { getCategoryById } from '../redux/actions/category.actions';
import { getShoppingCartProducts, removeFromShoppingCart, addQuantity } from '../redux/actions/shoppingCart.actions';
import { getCategories } from '../redux/actions/category.actions';

class CategoryPage extends Component {

    componentDidMount() {
        const { category_id } = this.props.match.params;
        this.props.getCategories();
        this.props.getCategoryById(category_id);
        this.props.getShoppingCartProducts();
        
    }

    removeProduct = (id) => {
        console.log("REMOVE");
        this.props.removeFromShoppingCart(id, this.props.shoppingCart.shoppingCartProducts);
    }

    addStock = (id, add) => {
        console.log("ADD QUANTITY");
        this.props.addQuantity(id, this.props.shoppingCart.shoppingCartProducts, add);
    }


    render() {
        return (
            <div>
                <Header />
                <div className='main-container'>
                    <div id="menu" className="close-menu">
                        <NavBar categories={this.props.categories.data} />
                    </div>
                    {this.props.categoryById &&
                        <CategoryContainer
                            category={this.props.categoryById.category}
                        />
                    }
                    <div id="shopping-cart" className="close-cart">
                        <ShoppingCart
                            products={this.props.shoppingCart.shoppingCartProducts}
                            removeProduct={this.removeProduct}
                            addStock={this.addStock}
                        />
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}


function mapStateToProps({ categoryById, shoppingCart, categories }) {
    return { categoryById, shoppingCart, categories }
}

export default connect(mapStateToProps, {
    getCategoryById, getShoppingCartProducts, removeFromShoppingCart, addQuantity, getCategories
})(CategoryPage);
