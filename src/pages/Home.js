import React, { Component } from 'react';
import Header from '../components/Header/Header';
import Category from '../components/Category/Category';
import Footer from '../components/Footer/Footer';
import NavBar from '../components/NavBar/NavBar';
import ShoppingCart from '../components/ShoppingCart/ShoppingCart';
import '../styles/Main.css';
import { connect } from 'react-redux';
import { getShoppingCartProducts, removeFromShoppingCart, addQuantity } from '../redux/actions/shoppingCart.actions';
import { getCategories } from '../redux/actions/category.actions';


class Home extends Component {
    componentDidMount() {
        this.props.getShoppingCartProducts();
        this.props.getCategories();
    }
    render() {
        console.log("categories home: ", this.props.categories);
        return (
            <div>

                <Header />
                <div className='home-container' >
                    <div id="menu" className="close-menu hide-for-mobile">
                        <NavBar categories={this.props.categories.data} />
                    </div>
                    <div className="categories">
                        {this.props.categories.data.map(category => (
                            <Category key={category.id} category={category} />
                        ))}
                    </div>
                    <div id="shopping-cart" className="close-cart">
                        <ShoppingCart
                            products={this.props.shoppingCart.shoppingCartProducts}
                            removeProduct={this.removeProduct}
                            addStock={this.addStock}
                        />
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}

function mapStateToProps({ shoppingCart, categories }) {
    return { shoppingCart, categories }
}

export default connect(mapStateToProps, {
    getShoppingCartProducts, removeFromShoppingCart, addQuantity, getCategories
})(Home);