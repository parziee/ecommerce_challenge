import types from './type';

export const getCategories = () => dispatch => {
    dispatch({
        type: types.GET_CATEGORIES
    })
}

export const getCategoryById = (category_id) => dispatch => {
    dispatch({
        type: types.GET_CATEGORY_BY_ID,
        category_id
    })
}