import types from './type';


export const getShoppingCartProducts = () => dispatch => {
    dispatch({
        type: types.GET_SHOPPING_CART_PRODUCTS
    })
}

export const addToShoppingCart = (product, shoppingCartProducts) => dispatch => {
    dispatch({
        type: types.ADD_TO_SHOPPING_CART,
        product,
        shoppingCartProducts
    })
}

export const removeFromShoppingCart = (id, shoppingCartProducts) => dispatch => {
    dispatch({
        type: types.REMOVE_FROM_SHOPPING_CART,
        id,
        shoppingCartProducts
    })
}

export const addQuantity = (id, shoppingCartProducts, add) => dispatch => {
    dispatch({
        type: types.ADD_QUANTITY,
        id,
        shoppingCartProducts,
        add
    })
}

