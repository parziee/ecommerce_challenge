import types from './type';

export const getProductsByCategory = (category_id) => dispatch => {
    dispatch({
        type: types.GET_PRODUCTS_BY_CATEGORY,
        category_id
    })
}

export const filterProducts = (evt, initialStateList) => dispatch => {
    dispatch({
        type: types.GET_PRODUCTS_BY_FILTER,
        evt,
        productList:initialStateList
    })
}

export const sortProducts = (evt, productList) => dispatch => {
    dispatch({
        type: types.GET_PRODUCTS_BY_SORT,
        evt,
        productList
    })
}

export const searchProducts = (evt, initialStateList) => dispatch => {
    dispatch({
        type: types.GET_PRODUCTS_BY_SEARCH,
        evt,
        initialStateList
    })
}