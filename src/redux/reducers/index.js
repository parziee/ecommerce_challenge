import { combineReducers } from 'redux';
import categoryById from './categoryById.reducer';
import productsByCategory from './productsByCategory.reducer';
import shoppingCart from './shoppingCart.reducer';
import categories from './categories.reducer';

export default combineReducers({
    categories,
    categoryById,
    productsByCategory,
    shoppingCart
});