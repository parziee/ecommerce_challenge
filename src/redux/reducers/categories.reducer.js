import type from '../actions/type';
import { categories } from '../../data/categories';

const initialState = {
    data: []
}

export default (state = initialState, action) => {
    console.log("categories: ", categories);
    switch (action.type) {
        case type.GET_CATEGORIES:
            return Object.assign({}, state, {
                data:categories
            });
        default:
            return state;
    }
}