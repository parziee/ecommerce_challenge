import type from '../actions/type';

const initialState = {
    data: []
}

export default (state = initialState, action) => {
    console.log("CARRITOO:: ", action);
    let shoppingCartProducts;
    switch (action.type) {

        case type.GET_SHOPPING_CART_PRODUCTS:
            shoppingCartProducts = [];
            if (localStorage.getItem('products')) {
                shoppingCartProducts = JSON.parse(localStorage.getItem('products'));
            }
            return Object.assign({}, state, {
                shoppingCartProducts
            });

        case type.ADD_TO_SHOPPING_CART:
            let product = Object.assign({}, action.product);
            console.log("action.shoppingCartProducts;", action.shoppingCartProducts);
            shoppingCartProducts = action.shoppingCartProducts;
            let flag = false;

            for (let p of shoppingCartProducts) {
                if (product.id === p.id) {
                    p.quantity++;
                    flag = true;
                }
            }
            if (!flag) {
                product.quantity = 1;
                shoppingCartProducts = [...shoppingCartProducts, product];
            }

            localStorage.setItem('products', JSON.stringify(shoppingCartProducts));

            return Object.assign({}, state, {
                shoppingCartProducts
            });

        case type.ADD_QUANTITY:
            shoppingCartProducts = action.shoppingCartProducts;
            for (let p of shoppingCartProducts) {
                if (action.id === p.id) {
                    if(action.add && p.quantity < 99){
                        p.quantity++;
                    }else if(!action.add && p.quantity > 1){
                        p.quantity--;
                    }
                }
            }
            //console.log("Products::::::::: ", shoppingCartProducts);
            localStorage.setItem('products', JSON.stringify(shoppingCartProducts));
            return Object.assign({}, state, {
                shoppingCartProducts
            });

        case type.REMOVE_FROM_SHOPPING_CART:
            shoppingCartProducts = action.shoppingCartProducts.filter(p => p.id != action.id);
            localStorage.setItem('products', JSON.stringify(shoppingCartProducts));
            return Object.assign({}, state, {
                shoppingCartProducts
            });

        default:
            return state;
    }
}
