import type from '../actions/type';
import { categories } from '../../data/categories';

const initialState = {
    data: {}
}

export default (state = initialState, action) => {
    console.log("action: ", action);
    switch (action.type) {
        case type.GET_CATEGORY_BY_ID:       
            const getCategory = (id, categories, result) => {
                for (let cat of categories) {
                    if (id == cat.id) {
                        result.push(cat);
                    } else {
                        if (cat.sublevels) {
                            getCategory(id, cat.sublevels, result);
                        }
                    }
                }
                return result
            }
            const category = getCategory(action.category_id, categories, [])[0];

            return Object.assign({}, state, {
                category
            });
        default:
            return state;
    }
}
