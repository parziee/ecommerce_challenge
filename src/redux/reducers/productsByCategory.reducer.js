import type from '../actions/type';
import { products } from '../../data/products';

const initialState = {
    productList: []
}

export default (state = initialState, action) => {
    let productList = [];
    let value;
    switch (action.type) {
        case type.GET_PRODUCTS_BY_CATEGORY:
            productList = products.filter(product => product.sublevel_id == action.category_id);
            return Object.assign({}, state, {
                productList: productList,
                initialState: productList
            });
        case type.GET_PRODUCTS_BY_FILTER:
            productList = action.productList;
            const available = action.evt.target.form.available.checked;
            const price_rank = action.evt.target.form.price.value;
            const stock = action.evt.target.form.stock.value;

            //available
            if (available) {
                productList = productList.filter(product => product.available);
            }

            //rank-price
            if (price_rank != '') {
                productList = productList.filter(product => {
                    let price = priceToNumber(product);
                    return price < price_rank && price > (price_rank - 10000);
                });
            }

            //Stock
            if (stock != 0) {
                productList = productList.filter(product => product.quantity >= stock);
            }

            return Object.assign({}, state, {
                productList: productList
            });
        case type.GET_PRODUCTS_BY_SORT:
            productList = action.productList;
            value = action.evt.target.value;
            if (value === "low_price") {
                productList.sort((a, b) => {
                    a = priceToNumber(a);
                    b = priceToNumber(b);
                    return a - b
                })
            } else if (value === "high_price") {
                productList.sort((a, b) => {
                    a = priceToNumber(a);
                    b = priceToNumber(b);

                    return b - a
                })
            } else if (value === "low_stock") {
                productList.sort((a, b) => {
                    return a.quantity - b.quantity
                })
            } else if (value === "high_stock") {
                productList.sort((a, b) => {
                    return b.quantity - a.quantity
                })
            }
            console.log("AFTERR: -> ", productList);

            return Object.assign({}, state, {
                productList: productList
            });

        case type.GET_PRODUCTS_BY_SEARCH:
            value  = action.evt.target.value;
            productList = action.initialStateList;
            productList = productList.filter(product => product.name.includes(value));
            return Object.assign({}, state, {
                productList: productList
            });
        default:
            return state;
    }
}

function priceToNumber(product) {
    let price = product.price.replace('$', '');
    price = Number(price.replace(',', ''));
    return price;
}

