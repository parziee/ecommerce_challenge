import React from 'react';
import { Row, Button, Collection, CollectionItem, Input, Icon } from 'react-materialize'
import './ShoppingCart.css';
import { connect } from 'react-redux';

class ShoppingCart extends React.Component {
    buy() {
        localStorage.setItem('products', JSON.stringify([]));
    }

    priceToNumber(price) {
        price = price.replace('$', '');
        price = Number(price.replace(',', ''));
        return price;
    }

    total() {
        let sum = 0;
        for (let p of this.props.products) {
            sum = sum + (p.quantity * this.priceToNumber(p.price));
        }
        return sum;
    }

    closeShoppingCart() {
        console.log("HOLIS");
        const element = document.getElementById('shopping-cart');
        element.classList.remove('open-cart');
        element.classList.add('close-cart');
    }

    render() {
        return (
            <div>
                {this.props.products &&
                    <form>
                        <div onClick={() => this.closeShoppingCart()} style={{ cursor: 'pointer' }}>
                            <Icon>close</Icon>
                        </div>
                        <Collection header='Buying List'>
                            {this.props.products.map(product => (
                                <CollectionItem key={product.id}>
                                    <Row>

                                        <div className="col product-name">{product.name}</div>
                                        <Button flat icon='expand_less' id="add" onClick={() => this.props.addStock(product.id, true)} />
                                        <span>{product.quantity}</span>
                                        <Button flat icon='expand_more' id="subtract" onClick={() => this.props.addStock(product.id, false)} />
                                        <span className='price'>{'$' + product.quantity * this.priceToNumber(product.price)}</span>
                                        <Button className="waves-effect waves-light red" icon='remove_shopping_cart' id="remove" onClick={() => this.props.removeProduct(product.id)} />
                                    </Row>
                                </CollectionItem>
                            ))}
                        </Collection>
                        <h4>Total: ${this.total()}</h4>
                        <Button className="buy_button" waves='light' large onClick={() => this.buy()}>Buy</Button>
                    </form>
                }
            </div>
        )
    }
}

function mapStateToProps({ shoppingCart }) {
    return {
        shoppingCart
    }
}

export default connect(mapStateToProps, {
})(ShoppingCart);
