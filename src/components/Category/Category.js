import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import './Category.css';

const urlBaseImage = '../../images/';

const CategoryBanner = styled(Link)`
    background:url(${props => urlBaseImage + props.category.id + '.jpg'}) no-repeat;
    background-size:cover;  
`

export default ({ category }) => {
    return (
        <CategoryBanner className='category-banner' to={`/categories/${category.id}`} category={category} >
            <div className="translucent">
                <h2 className="">{category.name}</h2>
            </div>
        </CategoryBanner>

    )
}

