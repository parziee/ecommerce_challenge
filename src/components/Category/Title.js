//import React from 'react';
import styled from 'styled-components';

const SectionTitle = styled.h4`
    background:rgba(245,245,245,1);
    color:black;
    margin:0; 
    padding:12px;
`;


export default SectionTitle;