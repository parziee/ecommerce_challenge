import React from 'react';
import styled from 'styled-components';
import ProductContainer from '../Product/ProductContainer';
import Title from './Title';
import { Link } from 'react-router-dom';
import './SubCategory.css';

const defaultImg = 'https://dummyimage.com/238x250.bmp/ff4444/ffffff';
const urlBaseImage = '../../images/';

const SubCategoryContainer = styled.div`
    display:flex;
    overflow:scroll;
`;

const CategoryElement = styled(Link)`
    background:url(${props => urlBaseImage + props.category.id + '.jpg'}), url(${defaultImg}) no-repeat;    
    background-size:cover;
    
`;

export default ({ category }) => (
    <div>
        <Title >{category.name}</Title>
        <ProductContainer
            category={category}
        />
        <Title >Other categories of {category.name}</Title>
        <SubCategoryContainer>
            {category.sublevels.map(subCategory => (
                <CategoryElement className="category-element"
                    key={subCategory.id}
                    to={`/categories/${subCategory.id}`}
                    onClick={() => this.props.getCategoryById(subCategory.id)}
                    category={subCategory}
                >
                    <div className="translucent" >
                        <p>{subCategory.name}</p>
                    </div>
                </CategoryElement>
            ))}
        </SubCategoryContainer>

    </div>

)


