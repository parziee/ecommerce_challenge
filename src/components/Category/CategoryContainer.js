import React from 'react';
import FinalCategory from './FinalCategory';
import SubCategory from './SubCategory';



export default ({ category }) => {
    if (category) {
        return (
            <div className="category-container" >
                {category.sublevels &&
                    <SubCategory 
                        category={category} 
                        key={category.id} 
                        />
                }
                {!category.sublevels &&
                    <FinalCategory 
                        category={category} 
                        key={category.id} 
                        />
                }
            </div>
        )
    } else {
        return (
            <div>Loading...</div>
        )
    }
}


