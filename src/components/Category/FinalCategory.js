import React from 'react';
import Title from './Title';
import ProductContainer from '../Product/ProductContainer';

class FinalCategory extends React.Component {

    render() {
        return (
            <div>
                <Title >{this.props.category.name}</Title>
                <ProductContainer 
                    isFinal={true} 
                    category={this.props.category} 
                    key={this.props.category.id} 
                    />
            </div>
        )
    }
}

export default FinalCategory;