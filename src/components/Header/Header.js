import React from 'react';
import './Header.css';
import Logo from '../../images/logo-rappi-new.svg';
import Cart from '../../images/shopping-cart.svg';
import { getShoppingCartProducts } from '../../redux/actions/shoppingCart.actions';
import { connect } from 'react-redux';
import { Icon } from 'react-materialize';

class Header extends React.Component {

    showMenu = () => {
        let element = document.getElementById('menu');
        if (element.classList.contains("close-menu")) {
            element.classList.remove("close-menu");
            element.classList.add("open-menu");
            if(document.getElementById('shopping-cart').classList.contains('open-cart')){
                this.showShoppingCart();
            }
        } else {
            element.classList.remove("open-menu");
            element.classList.add("close-menu");
        }
    }


    showShoppingCart = () => {
        let element = document.getElementById('shopping-cart');
        if (element.classList.contains("close-cart")) {
            element.classList.remove("close-cart");
            element.classList.add("open-cart");
            if(document.getElementById('menu').classList.contains('open-menu')){
                this.showMenu();
            }
        } else {
            element.classList.remove("open-cart");
            element.classList.add("close-cart");
        }
    }

    render() {
        console.log("this.props.shoppingCart.shoppingCartProducts: ", this.props.shoppingCart.shoppingCartProducts);
        return (
            <div>
                <header>
                    <div className="menu-icon" onClick={this.showMenu} style={{cursor:'pointer'}}>
                        <Icon small>menu</Icon>
                    </div>
                    <a href="/"><img className="logo" src={Logo} alt="Logo Rappi" width="80" /></a>
                    <a className="shopping-cart" onClick={this.showShoppingCart} href="#shopping-cart">
                        <img className="shopping-cart__icon" src={Cart} width="30" alt="Shopping Cart" align="right" />
                        {this.props.shoppingCart.shoppingCartProducts  &&
                            <div className="shopping-cart__number">{this.props.shoppingCart.shoppingCartProducts.length}</div>
                        }
                    </a>
                </header>
            </div>
        )
    }
}

function mapStateToProps({ shoppingCart }) {
    return {
        shoppingCart
    }
}

export default connect(mapStateToProps, {
     getShoppingCartProducts
})(Header);






