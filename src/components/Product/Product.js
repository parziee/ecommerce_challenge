import React from 'react';
import styled from 'styled-components';
import { Button } from 'react-materialize';
import './Product.css';

const defaultImg = 'http://dummyimage.com/213x213.png/dddddd/000000';
const urlBaseImage = '../../images/products/';

const Container = styled.div`
    .product-photo{
        background:url(${props => urlBaseImage+props.product.id+'.jpg'}), url(${defaultImg}) no-repeat;
        background-size:cover;
    }
`;

export default ({ product, addToShoppingCart }) => {
    return (
        <Container className="product-container" product={product}>
            <div className="product-photo">
                {<div className="translucent">
                    <Button disabled={!product.available} floating onClick={() => addToShoppingCart(product)} icon='add_shopping_cart' className='yellow darken-1' />
                </div>}
            </div>
            <p className="price">{product.price}</p>
            <p className="name">{product.name}</p>
            <p className="quantity">quantity: {product.quantity}</p>
            {!product.available &&
                <p className="not-available">NOT AVAILABLE</p>
            }
        </Container>
    )
}