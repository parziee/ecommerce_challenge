import React from 'react';
import ProductCarousel from './ProductCarousel';
import FilterBar from '../Filters/FilterBar';
import SearchBar from '../Filters/SearchBar';
import { connect } from 'react-redux';
import { getProductsByCategory, filterProducts, sortProducts, searchProducts } from '../../redux/actions/product.actions';
import { getShoppingCartProducts, addToShoppingCart, removeFromShoppingCart, addQuantity } from '../../redux/actions/shoppingCart.actions';


class ProductContainer extends React.Component {
    state = {
        productsShoppingCart: []
    }
    componentDidMount() {
        //this.getProductsByCategory(this.props.category.id);
        this.props.getProductsByCategory(this.props.category.id);
    }

    filterProducts = evt => {
        let initialStateList = this.props.productsByCategory.initialState;
        this.props.filterProducts(evt, initialStateList);
    }

    sortProducts = evt => {
        let productList = this.props.productsByCategory.productList;
        this.props.sortProducts(evt, productList);
    }

    search = evt => {
        let initialStateList = this.props.productsByCategory.initialState;
        this.props.searchProducts(evt, initialStateList);
    }

    addToShoppingCart = (product) => {
        console.log("ADD::: ", this.props.shoppingCart.shoppingCartProducts)    ;
        this.props.addToShoppingCart(product, this.props.shoppingCart.shoppingCartProducts);
    }

    render() {
        return (
            <div>
                
                <FilterBar
                    filterProducts={this.filterProducts}
                    sortProducts={this.sortProducts}
                />
                {this.props.isFinal &&
                    <SearchBar search={this.search} />
                }
                {this.props.productsByCategory.productList &&
                    <ProductCarousel
                        products={this.props.productsByCategory.productList}
                        addToShoppingCart={this.addToShoppingCart}
                    />
                }
                {this.props.productsByCategory.productList == 0 &&
                    <p>No Product Results...</p>
                }
            </div>
        )
    }
}

function mapStateToProps({ productsByCategory, shoppingCart }) {
    return {
        productsByCategory, shoppingCart
    }
}

export default connect(mapStateToProps, {
    getProductsByCategory, 
    filterProducts, 
    sortProducts, 
    searchProducts, 
    getShoppingCartProducts, 
    addToShoppingCart, 
    removeFromShoppingCart,
    addQuantity,
})(ProductContainer);

