import React from 'react';
import styled from 'styled-components';
import Product from './Product';

const Container = styled.div`
    display:flex;
    overflow:scroll;    
`;

export default ({ products, addToShoppingCart }) => (
    <Container>
        {products.map(product => (
            <Product key={product.id} product={product} addToShoppingCart={addToShoppingCart} />
        ))}
    </Container>
)
