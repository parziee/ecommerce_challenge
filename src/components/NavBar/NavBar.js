import React from 'react';
import SubNavBar from './SubNavBar'
import './NavBar.css'



export default ({ categories }) => (
    <div >
        <ul>
            {categories.map(category => (
                <li className="navbar-element" key={category.id}>
                    <SubNavBar category={category} />
                </li>
            ))}
        </ul>
    </div>
);

