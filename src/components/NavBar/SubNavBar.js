import React, { Component } from 'react';
import NavBar from './NavBar';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {getCategoryById} from '../../redux/actions/category.actions';


class SubNavBar extends Component {

    showSublevels(category, e) {
        let element = document.getElementById(category.id);
        if (element.classList.contains("close")) {
            element.classList.remove("close");
            element.classList.add("open");
        } else {
            element.classList.remove("open");
            element.classList.add("close");
        }
        this.props.getCategoryById(category.id);
    }

    render() {
        return (
            <div className='sub-navbar' >
                <Link 
                    onClick={(e) => this.showSublevels(this.props.category, e)} 
                    key={this.props.category.id}
                    to={{pathname:`/categories/${this.props.category.id}`,state:{id: this.props.category.id}}}>
                    {this.props.category.name}</Link>
                <div className='close' id={this.props.category.id}>
                    {this.props.category.sublevels &&
                        <NavBar categories={this.props.category.sublevels} />}
                </div>
            </div>
        );

    }
}

function mapStateToProps({ categoryById }) {
    return {
        categoryById
    }
}

export default connect(mapStateToProps, {
    getCategoryById
})(SubNavBar);