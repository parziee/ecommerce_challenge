import React from 'react';
import './Footer.css';

export default () => (
    <footer>
        <div className="copyright">© 2018 Rappi</div>
    </footer>
);
