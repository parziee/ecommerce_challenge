import React from 'react';
import styled from 'styled-components';
import {Row, Input} from 'react-materialize';

const SearchBar = styled.div`
.input-field{
    background:rgba(245,245,245,1);
}
`

export default({search})=>(
    <Row>
        <SearchBar>
        <i className="fa fa-search fa-lg" aria-hidden="true"></i>
        <Input s={12} placeholder="Search product..." onChange={search}/>
        </SearchBar>
    </Row>
)