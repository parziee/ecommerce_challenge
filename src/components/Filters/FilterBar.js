import React from 'react';
import styled from 'styled-components';
import { Row, Input } from 'react-materialize'

const FilterSection = styled.div`
    background:rgba(245,245,245,1);
    margin:0;
    .col:not(:first-child){
        margin-top:10px;
        
    }
`;


export default ({ filterProducts, sortProducts }) => {
    return (
        <FilterSection className="filter-section">
            <form>
                <Row>
                    <Input s={12} name='filters' id='available' type='checkbox' value='available' label='only available products' className='filled-in' onChange={filterProducts} />
                    <Input s={4} type='select' id='price' label="price rank" defaultValue='2' onChange={filterProducts}>
                        <option value=''></option>
                        <option value='10000'>0 - 10000</option>
                        <option value='20000'>10000 - 20000</option>
                        <option value='30000'>20000 - 30000</option>
                        <option value='40000'>40000 - 50000</option>
                        <option value='50000'>50000 - 50000</option>
                    </Input>
                    <Input name='stock' id='stock' type='number' label='stock' min='0' max='1000' step='50' onChange={filterProducts} />
                    <Input s={4} type='select' id='sort' label="Sort by" defaultValue='2' onChange={sortProducts}>
                        <option value=''></option>
                        <option value='low_price'>Price: Low to High</option>
                        <option value='high_price'>Price: High to Low</option>
                        <option value='low_stock'>Stock: Low to High</option>
                        <option value='high_stock'>Stock: High to Low</option>
                    </Input>
                </Row>
            </form>

        </FilterSection>
    )
};