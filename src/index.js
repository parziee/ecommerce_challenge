import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import './styles/Main.css';
import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import { Provider } from 'react-redux';
import reducers from './redux/reducers';
import reduxThunk from 'redux-thunk';
import promise from 'redux-promise-middleware';

const store = createStore(reducers, {}, applyMiddleware(promise(), reduxThunk,logger));

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root'));
registerServiceWorker();
